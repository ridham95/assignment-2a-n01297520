﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTML.aspx.cs" Inherits="WebApplication1.HTML" %>
<asp:Content ID="Content1" ContentPlaceHolderID="first" runat="server">
    <h1 class="head1"> IMPORTANT LINKS</h1>
    <nav id="list1">
    
        <ul class="links">
<li><a href="www.google.com">GOOGLE</a></li>
<li><a href="www.w3schools.com">W3SCHOOLS</a></li>
            <li><a href="www.teamtreehouse.com">TEAM TREEHOUSE</a></li>
            <li><a href="www.dreamacademy.com">DREAM ACADEMY</a></li>

        </ul>
    </nav>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="second" runat="server">
    
    <h2 class="head2">NOTES</h2>
    <p class="para2">
        HTML is also known as Hypertext Markup Language. It helps us to create a website. HTML elements are the building
        blocks of the HTML pages. HTML elements are represented by tags. The HTML tags normally comes in pairs. The first
        is the start tag and second is the end tag. HTML is a fun language to learn.
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="third" runat="server">
    <h3 class="head3"> CODE</h3>
    <p class="para3">
        <!DOCTYPE html>
        <html>
            <head>
                <title> TITLE PAGE</title>
            </head>
            <body>
                <p>My first Paragraph.</p>
            </body>
        </html>
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="fourth" runat="server">
    <div id="myclass">
    <h4 class="head4"> Learning Outcome.</h4>
    <p class="para4">
        HTML makes us learn to how to modify a website in a presentable manner. It is a fun to learn language.
        
    </p></div>
</asp:Content>
