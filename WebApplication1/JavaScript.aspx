﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="WebApplication1.JavaScript" %>
<asp:Content ID="Content1" ContentPlaceHolderID="first" runat="server">
     <h1 class="head1"> IMPORTANT LINKS</h1>
    <nav id="list1">
    <ul id="links">
<li><a href="www.google.com">GOOGLE</a></li>
<li><a href="www.w3schools.com">W3SCHOOLS</a></li>
            <li><a href="www.planetjavascript.com">TEAM TREEHOUSE</a></li>
            <li><a href="javascripttutorial.com">DREAM ACADEMY</a></li>
  </ul>
   </nav>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="second" runat="server">
    <h2 class="head2">INTRODUCTION</h2>
    <p class="para2">
        Javascript is the programming language of the HTML and the web. It is an easy to learn language. Javascript
        helps us to learn the behavior of the webpages.
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="third" runat="server">
    <h3 class="head3">CODE</h3>
    <p class="para3">
        <!DOCTYPE html>
<html>
<body>

<h6>My First JavaScript</h6>

<button type="button"
onclick="document.getElementById('demo').innerHTML = Date()">
Click me to display Date and Time.</button>

<p id="demo"></p>

</body>
</html> 
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="fourth" runat="server">
    <div id="myclass">
    <h4 class="head4">LEARNING OUTCOME</h4>
    <p class="para4">
        Explain separation of concerns and identify the three layers of the web.
Use operators, variables, arrays, control structures, functions and objects in JavaScript.
Map HTML using the DOM - Document Object Model.
    </p></div>
</asp:Content>
